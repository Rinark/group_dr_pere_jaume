#include "Game.h"

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT) {
	timer = 0;
	done = false;
	select = 0;
}

/**
* Destructor
*/
Game::~Game() {
}

/*
* Game execution
*/
void Game::run() {
	//Prepare the game components
	init();
	//Start the game if everything is ready
	gameLoop();
}


/*
* Initialize all the components
*/
void Game::init() {
	//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);
	_graphic.setWindowBackgroundColor(255, 255, 255, 255);
	//Load the sprites associated to the different game elements
	_graphic.loadTexture(SPRITE_HERO, "../sharedResources/images/characters/hero.png");
	_graphic.loadTexture(SPRITE_FIRE, "../sharedResources/images/characters/fireSprite.png");
	_graphic.loadTexture(SPRITE_GRASS, "../sharedResources/images/terrain/grass1.bmp");
	_graphic.loadTexture(SPRITE_WALL, "../sharedResources/images/terrain/snow1.bmp");//farem servir la neu com a mur provisionalment.
	_graphic.loadTexture(SPRITE_BOSS, "../sharedResources/images/characters/monster.bmp");
	_graphic.loadTexture(SPRITE_TREE, "../sharedResources/images/objects/PineTree.png");
	_graphic.loadTexture(SPRITE_BOTI, "../sharedResources/images/objects/first_aid_kit.png");
	_graphic.loadTexture(SPRITE_ARMOR, "../sharedResources/images/objects/wepicon-helm.bmp");
	 //Set the font style
	_graphic.setFontStyle(TTF_STYLE_NORMAL);
	//Initialize the game elements
}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {
	_gameState = GameState::MENU;

	while (_gameState != GameState::EXIT) {
		if (_gameState == GameState::DESTROY){
			mapa.~Map();
			_gameState = GameState::MENU;
		}
		if (_gameState == GameState::INIT_PLAY){
			done = false;
			mapa.populateMap();
			_gameState = GameState::PLAY;
		}
		//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
		//Execute the player commands 
		executePlayerCommands();
		//Update the game physics
		doPhysics();
		//Render game
		renderGame();
		//check conditions to win/lose
		if(_gameState== GameState::PLAY)checkWinLose(mapa.getHeroPointer());
	}
}

/*
Comprobem les condicions per a Guanyar/perdre
*/
void Game::checkWinLose(Player* pl) {
	if (pl->GetHealth()<=0){
		_gameState = GameState::LOSE;
		if (timer>100)done = true;
		timer++;
	}
	else if((pl->GetInventorySize())>=3){
		_gameState = GameState::WIN;
		if (timer>100)done = true;
		timer++;
	}
}

/*
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {
	if (_graphic.isKeyPressed(SDL_BUTTON_LEFT)) {
		glm::ivec2 mouseCoords = _graphic.getMouseCoords();
	}
	if (_gameState == GameState::MENU) {
		//EL BOTO RETURN ES EL ENTER.
		if (_graphic.isKeyPressed(SDLK_RETURN)) {
			if (select == 0) {
				select = 0;
				_gameState = GameState::INIT_PLAY;
			} else if (select == 1) {
				select = 0;
				mapa.loadMap();
				_gameState = GameState::PLAY;
			}
			else if (select == 2) {
				select = 0;
				ranking.loadRanking();
				_gameState = GameState::L_RANKING;
			}
			else {
				select = 0;
				mapa.~Map();
				_gameState = GameState::EXIT;
			}
		}
		if (_graphic.isKeyPressed(SDLK_s)) {
			ranking.initRanking();
		}

		if (_graphic.isKeyPressed(SDLK_DOWN)) {//si esta al menu cambiem la sel�leccio
			select++;
			if (select > 2)select = 0;
		}

		if (_graphic.isKeyPressed(SDLK_UP)) {//si esta al menu cambiem la sel�leccio
			select--;
			if (select < 0)select = 2;
		}
	}

	else if (_gameState == GameState::L_RANKING) {
		if (_graphic.isKeyPressed(SDLK_RETURN)) {
			_gameState = GameState::MENU;
		}
	}

	else if (_gameState == GameState::S_RANKING) {
		if (_graphic.isKeyPressed(SDLK_RETURN)) {
			if(!ranking.doneName()) ranking.saveUserName();
			else {
				ranking.saveRanking();
				ranking.resetUser();
				_gameState = GameState::DESTROY;
			}
		}
		if (_graphic.isKeyPressed(SDLK_DOWN)) {//si esta al ranking cambiem la sel�leccio
			ranking.setPos(-1);
		}

		if (_graphic.isKeyPressed(SDLK_UP)) {//si esta al ranking cambiem la sel�leccio
			ranking.setPos(+1);
		}
	}

	else if (_gameState == GameState::PLAY) {
		Player* heroe = mapa.getHeroPointer();
		if (_graphic.isKeyPressed(SDLK_UP)) {
			int tempx = heroe->GetPosX();
			int tempy = heroe->GetPosY() - 1;
			if (mapa.checkCollisions(tempx, tempy, 1)) { //comproba si la seguent cel�la es un mur, sino deixa avan�ar al jugador
				heroe->SetPosY(tempy);
			}
		}

		if (_graphic.isKeyPressed(SDLK_LEFT)) {
			int tempx = heroe->GetPosX() - 1;
			int tempy = heroe->GetPosY();
			if (mapa.checkCollisions(tempx, tempy, 1)) {//comproba si la seguent cel�la es un mur, sino deixa avan�ar al jugador
				heroe->SetPosX(tempx);
			}
		}

		if (_graphic.isKeyPressed(SDLK_DOWN)) {
			int tempx = heroe->GetPosX();
			int tempy = heroe->GetPosY() + 1;
			if (mapa.checkCollisions(tempx, tempy, 1)) {//comproba si la seguent cel�la es un mur, sino deixa avan�ar al jugador
				heroe->SetPosY(tempy);
			}
		}

		if (_graphic.isKeyPressed(SDLK_RIGHT)) {
			int tempx = heroe->GetPosX() + 1;
			int tempy = heroe->GetPosY();
			if (mapa.checkCollisions(tempx, tempy, 1)) {//comproba si la seguent cel�la es un mur, sino deixa avan�ar al jugador
				heroe->SetPosX(tempx);
			}
		}

		if (_graphic.isKeyPressed(SDLK_m)) {
			_gameState = GameState::MENU;
		}

		if (_graphic.isKeyPressed(SDLK_q)) {
			if (heroe->GetInventorySize()>0){
				heroe->RemoveElementInventory();
				heroe->SetHealth(25);
			}
		}
		if (_graphic.isKeyPressed(SDLK_s)){
			mapa.saveGame();
		}
	}

	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		mapa.~Map();
		_gameState = GameState::EXIT;
	}
}

/*
* Execute the game physics
*/
void Game::doPhysics() {
	if (_gameState == GameState::MENU) {

	}
	else {
		mapa.animateObjects();
		mapa.animateCharacters();
	}
}

/*
* Calls the function that draws the corresponding screen to each state.
*/
void Game::renderGame() {
	if (_gameState == GameState::PLAY) {
		renderActiveGame();
	}
	else if (_gameState == GameState::MENU){
		renderMenu();
	}
	else if (_gameState == GameState::LOSE) {
		renderLose();
	}
	else if (_gameState == GameState::WIN) {
		renderWin();
	}
	else if(_gameState == GameState::L_RANKING){
		ranking.drawL_Ranking(_graphic, _screenWidth, _screenHeight);
	}
	else if (_gameState == GameState::S_RANKING) {
		ranking.drawS_Ranking(_graphic, _screenWidth, _screenHeight);
	}
}

/*
Pinta la pantalla de Guanyar
*/
void Game::renderWin() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw BG
	_graphic.drawFilledRectangle(BLACK, 0, 0, _screenWidth, _screenHeight);

	//Draw title
	_graphic.drawText("YOU WIN", WHITE, BLACK, (_screenWidth / 2) - 125, 50, 100);


	//Refresh screen
	_graphic.refreshWindow();

	ranking.setScore(mapa.getHeroPointer()->GetScore());
	_gameState = GameState::S_RANKING;
}

/*
Pinta la pantalla de Perdre
*/
void Game::renderLose() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw BG
	_graphic.drawFilledRectangle(BLACK, 0, 0, _screenWidth, _screenHeight);

	//Draw title
	_graphic.drawText("YOU LOSE", WHITE, BLACK, (_screenWidth / 2) - 125,(_screenHeight/2), 100);

	//Refresh screen
	_graphic.refreshWindow();

	_gameState = GameState::EXIT;
}

/*
Pinta el mapa m�s la UI.
*/
void Game::renderActiveGame() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw the map
	mapa.renderMap(_graphic);

	//Draw UI elements
	Player* heroe = mapa.getHeroPointer();

	_graphic.drawFilledRectangle(BLACK, 0, 0, _screenWidth, (_screenHeight / 12));
	string plHP = "Player HP: " + to_string(heroe->GetHealth());
	_graphic.drawText(plHP, WHITE, BLACK, 0, 0, 0);
	string plSC = "Player Score: " + to_string(heroe->GetScore());
	_graphic.drawText(plSC, WHITE, BLACK, _screenWidth - 175, 0, 0);
	string plARM = "Armor: " + to_string(heroe->GetArmor());
	_graphic.drawText(plARM, WHITE, BLACK, 0, 20, 0);
	string plBot = "Botiquin: " + to_string(heroe->GetInventorySize());
	_graphic.drawText(plBot, WHITE, BLACK, _screenWidth - 175, 20, 0);

	
	//Refresh screen
	_graphic.refreshWindow();
}

/*
Pinta el menu principal.
*/
void Game::renderMenu() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw menu BG
	_graphic.drawFilledRectangle(BLACK, 0, 0, _screenWidth, _screenHeight);

	//Draw Menu title
	_graphic.drawText("FIRE HERO", WHITE, BLACK, (_screenWidth / 2) - 125, 10, 100);

	//Draw menu buttons
	if (select==0) {
		_graphic.drawText("PLAY", WHITE, GREEN, 300, 300, 0);
		_graphic.drawText("LOAD GAME", WHITE, BLACK, 300, 350, 0);
		_graphic.drawText("RANKING", WHITE, BLACK, 300, 400, 0);
		_graphic.drawText("EXIT", WHITE, BLACK, 300, 450, 0);
	}
	else if(select == 1){
		_graphic.drawText("PLAY", WHITE, BLACK, 300, 300, 0);
		_graphic.drawText("LOAD GAME", WHITE, GREEN, 300, 350, 0);
		_graphic.drawText("RANKING", WHITE, BLACK, 300, 400, 0);
		_graphic.drawText("EXIT", WHITE, BLACK, 300, 450, 0);
	} else if (select == 2) {
		_graphic.drawText("PLAY", WHITE, BLACK, 300, 300, 0);
		_graphic.drawText("LOAD GAME", WHITE, BLACK, 300, 350, 0);
		_graphic.drawText("RANKING", WHITE, GREEN, 300, 400, 0);
		_graphic.drawText("EXIT", WHITE, BLACK, 300, 450, 0);
	}
	else {
		_graphic.drawText("PLAY", WHITE, BLACK, 300, 300, 0);
		_graphic.drawText("LOAD GAME", WHITE, BLACK, 300, 350, 0);
		_graphic.drawText("RANKING", WHITE, BLACK, 300, 400, 0);
		_graphic.drawText("EXIT", WHITE, GREEN, 300, 450, 0);
	}
	//Refresh screen
	_graphic.refreshWindow();
}



/*
* Draw the sprite on the screen
* @param sprite is the sprite to be displayed
*/
void Game::drawSprite(Sprite & sprite) {
	_graphic.drawTexture(sprite.getSpriteId(), SPRITE_DEFAULT_WIDTH*sprite.getCurrentFrame(), 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT
		, sprite.getXAtWorld(), sprite.getYAtWorld(), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
}


