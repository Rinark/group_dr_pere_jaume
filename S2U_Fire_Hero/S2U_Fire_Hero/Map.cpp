#include "Map.h"
#include <iostream>
#include <string>
#include <string.h>
using namespace std;

Map::Map() {
		for (int i = 0; i < MAX_ROW_BLOCK; i++) {
			for (int j = 0; j < MAX_COL_BLOCK; j++) {
				cells[i][j].minx = 64 * i;
				cells[i][j].miny = 64 * j;
				cells[i][j].SpriteType = SPRITE_GRASS;
			}
		}
}

Map::~Map() {
	cout << "Destroying Map" << endl;
}

/*
Retorna la ID de la casella
*/
int Map::getCellType(int x, int y){
	return cells[x][y].SpriteType;
}

/*
Retorna la POSX de la casella
*/
int Map::getCellPosX(int x, int y){
	return cells[x][y].minx;
}

/*
Retorna la POSY de la casella
*/
int Map::getCellPosY(int x, int y){
	return cells[x][y].miny;
}

/*
Guarda la partida
*/
void Map::saveGame() {
	std::ofstream saveFile;
	saveFile.open("saveFile.txt", std::ios::out | std::ios::trunc);

	if (!saveFile.is_open()) {
		throw std::exception("[saveFile] System was not able to open the file");
	}

	obList.beginIteration();
	while (!obList.endIteration()) {
		Object* objecte;
		objecte = obList.nextElement();
		saveFile << objecte->GetID() << '/' << objecte->GetPosX() << '/' << objecte->GetPosY() << '/' << endl;
	}

	chList.beginIteration();
	while (!chList.endIteration()) {
		Character* objecte;
		objecte = chList.nextElement();
		if (objecte->GetID() == SPRITE_HERO) {
			saveFile << objecte->GetID() << '/' << objecte->GetPosX() << '/' << objecte->GetPosY() << '/' << objecte->GetHealth() << '/' << objecte->GetArmor() << '/' << chList.GetHero()->GetLifes() << '/' << chList.GetHero()->GetScore() << '/';
			for (int i = 0; i < objecte->GetInventorySize(); i++) {
				saveFile << objecte->ReturnElementInventory(i) << '/';
			}
			saveFile << '|' << endl;
		}
		else if (objecte->GetID() == SPRITE_FIRE) {
			saveFile << objecte->GetID() << '/' << objecte->GetPosX() << '/' << objecte->GetPosY() << '/' << objecte->GetHealth() << '/' << endl;
		}
		else {
			saveFile << '/' << objecte->GetID() << '/' << objecte->GetPosX() << '/' << objecte->GetPosY() << '/' << objecte->GetHealth() << '/' << endl;
		}
		//NOTA PARA MI YO DEL FUTURO: Potser necessitem maxframes
	}
	saveFile << MAP_ID << '/' << recorrerMapa() << '/';
	saveFile.close();
}

/*
Emplena les llistes respectives de objectes i personatges
*/
void Map::populateMap() {
	cout << "Adding stuff to lists" << endl;

	constructWalls();

	Object* obj;
	obj = new Botiquin(6, 6, 1);
	obList.addElement(obj);
	obj = new Botiquin(8, 8, 1);
	obList.addElement(obj);
	obj = new Botiquin(7, 5, 1);
	obList.addElement(obj);
	obj = new Armadura(16, 16, 1);
	obList.addElement(obj);
	obj = new Arbre(11, 11, 1);
	obList.addElement(obj);

	Character* charc;
	charc = new Player(100, 3, 10, 10, 1);
	chList.addElement(charc);
	charc = new Enemic(10, 5, 5, 5);
	chList.addElement(charc);
	charc = new Enemic(10, 15, 15, 5);
	chList.addElement(charc);
}

/*
Afegeix els murs exteriors a la object list
*/
void Map::constructWalls() {
	for (int i = 0; i < MAX_ROW_BLOCK; i++) {
		for (int j = 0; j < MAX_COL_BLOCK; j++) {
			if (i == 0 || i == (MAX_ROW_BLOCK - 1) || j == 0 || j == (MAX_COL_BLOCK - 1)) {
				Object* obj;
				obj = new Wall(i, j, 1);
				obList.addElement(obj);
			}
		}
	}
}

/*
Check the collisions inside the game and return true if the next tile is walkable or
false if you can't advance through it.
*/
bool Map::checkCollisions(int x, int y, int z) {
	int ID = 69;
	/*
	Recorremos la lista de objetos para comprobar si la proxima posicion contiene alguno.
	En caso positivo guardamos la ID del objeto que esta en la siguiente casilla.
	Y si es un objeto que se peude recoger llamamos a la funcion de a�adir al inventario, lo destruimos
	y lo eliminamos de la lista.
	*/
	obList.beginIteration();
	while (!obList.endIteration()) {
		Object* objecte;
		objecte = obList.nextElement();

		if (objecte->GetPosX() == x && objecte->GetPosY() == y) {
			ID = objecte->GetID();
			if (ID != SPRITE_WALL && ID != SPRITE_TREE) {
				int obID = objecte->GetID();
				executeObject(obID, objecte);
				break;
			}
		}
	}
	/*
	Recorremos la lista de characters para comprobar si la proxima posicion contiene alguno.
	En caso positivo guardamos la ID del character que esta en la siguiente casilla.
	*/
	chList.beginIteration();
	while (!chList.endIteration()) {
		Character* objecte;
		objecte = chList.nextElement();

		if (objecte->GetPosX() == x && objecte->GetPosY() == y) {
			ID = objecte->GetID();
		}
	}
	/*
	Devolvemos true o false segun la ID del objeto y si este puede caminar por encima.
	*/
	if (ID == SPRITE_WALL || ID == SPRITE_TREE) {
		return false;
	}
	else if (ID == SPRITE_FIRE || ID == SPRITE_BOSS) {
		(chList.GetHero())->SetHealth(-20);
		return false;
	}
	else {
		return true;
	}
}

/*
Carrega la informacio de la partida.
*/
void Map::loadMap() {
	string tempString;
	string line;
	std::ifstream saveFile;
	saveFile.open("saveFile.txt", std::ios::in);
	if (!saveFile.is_open()) {
		throw std::exception("[saveFile] System was not able to open the file");
	}
	for (int i = 0; std::getline(saveFile, line); ++i) {
		tractarString(line);
	}
	saveFile.close();
}

void Map::tractarString(string line) {
	int i = 2;
	char id = line[0];
	string tempString;
	Character* charc;
	Object* elem;
	int posx;
	int posy;
	int health;
	int lifes;
	int armor;
	int score;
	switch (id) {
	case '0':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		health = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		armor = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		lifes = stoi(tempString);
		charc = new Player(health, lifes, posx, posy, 1);
		chList.addElement(charc);
		chList.GetHero()->SetArmor(armor);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		score = stoi(tempString);
		chList.GetHero()->SetScore(score);
		i++;
		tempString = "";
		for (i; line[i] != '|'; i++) {
			if (line[i] == '/') {
				elem = new Botiquin(1, 1, 1);
				chList.GetHero()->AddElementInventory(elem);
			}
			else {
				tempString += line[i];
			}

		}
		break;
	case '1':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		health = stoi(tempString);
		charc = new Enemic(health, posx, posy, 5);
		chList.addElement(charc);
		break;
	case '2':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		elem = new Wall(posx, posy, 1);
		obList.addElement(elem);
		break;
	case '4':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		health = stoi(tempString);
		charc = new Boss(health, posx, posy, 5);
		chList.addElement(charc);
		break;
	case '5':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		elem = new Arbre(posx, posy, 1);
		obList.addElement(elem);
		break;
	case '6':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		elem = new Botiquin(posx, posy, 1);
		obList.addElement(elem);
		break;
	case '7':
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posx = stoi(tempString);
		i++;
		tempString = "";
		for (i; line[i] != '/'; i++) {
			tempString += line[i];
		}
		posy = stoi(tempString);
		elem = new Armadura(posx, posy, 1);
		obList.addElement(elem);
		break;
	}

}

/*
Exectues the code for each tipe of object
*/
void Map::executeObject(int obID, Object* objecte) {
	switch (obID) {
	case SPRITE_ARMOR:
		chList.GetHero()->SetArmor(10);
		chList.GetHero()->SetScore(5);
		objecte->spawn();
		break;
	case SPRITE_BOTI:
		chList.GetHero()->AddElementInventory(objecte);
		chList.GetHero()->SetScore(10);
		objecte->spawn();
		break;
	default:
		break;
	}
}

/*
Pinta el mapa.
*/
void Map::renderMap(SDLInterface & _graphic) {
	int maxX = 6;
	int maxY = 6;
	int minY = 4;
	int minX = 4;
	Player* heroe = chList.GetHero();

	int z = 0, w = 0;//Posicions de les cel�les de la pantalla
	for (int zi = 0; zi < 4; zi++) {//recorre les capes del mapa
		for (int i = heroe->GetPosX() - minX; i <= (heroe->GetPosX() + maxX); i++) {//recorre les x a partir de la posicio del jugador x -5 fins a +5 per a pintar 10 caselles
			for (int j = heroe->GetPosY() - minY; j <= (heroe->GetPosY() + maxY); j++) {//recorre les y a partir de la posicio del jugador y -5 fins a +5 per a pintar 10 caselles
				if (i < 0) {
					maxX++;
					i = 0;
				}
				if (j < 0) {
					maxY++;
					j = 0;
				}
				if (i > MAX_COL_BLOCK) {
					maxX--;
					minX++;
					i = MAX_COL_BLOCK;
				}
				if (j > MAX_ROW_BLOCK) {
					maxY--;
					minY++;
					j = MAX_ROW_BLOCK;
				}
				if (zi < 2) {
					if (getCellType(i, j) == SPRITE_WALL) {//mirem el tipus de ce�la i pintem el que toca
						_graphic.drawTexture(SPRITE_WALL, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, 64 * w, 64 * z, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
					}
					else {
						_graphic.drawTexture(SPRITE_GRASS, 0, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, 64 * w, 64 * z, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
					}
					z++;
				}
				else if (zi == 2) {
					renderObjects(i, j, w, z, _graphic);
					z++;
				}
				else if (zi == 3) {
					renderCharacters(i, j, w, z, _graphic);
					z++;
				}
			}
			z = 0;
			w++;
		}
		w = 0;
	}
}

/*
Recorre la llista de characters i fa que es pintin.
*/
void Map::renderCharacters(int i, int j, int w, int z, SDLInterface& _graphic) {
	chList.beginIteration();
	while (!chList.endIteration()) {
		Character* charac;
		charac = chList.nextElement();

		if (charac->GetPosX() == i && charac->GetPosY() == j) {
			charac->draw(_graphic, w, z);
		}
	}
}

/*
Recorre la llista de objects i fa que es pintin.
*/
void Map::renderObjects(int i, int j, int w, int z, SDLInterface& _graphic) {
	obList.beginIteration();
	while (!obList.endIteration()) {
		Object* objecte;
		objecte = obList.nextElement();

		if (objecte->GetPosX() == i && objecte->GetPosY() == j) {
			objecte->draw(_graphic, w, z);
		}
	}
}

/*
Returns a pointer to Player
*/
Player * Map::getHeroPointer(){
	return chList.GetHero();
}

/*
Anima els objects, augmentant la variable de frame actual.
*/
void Map::animateObjects() {
	obList.beginIteration();
	while (!obList.endIteration()) {
		Object* objecte;
		objecte = obList.nextElement();
		objecte->nextFrame();
	}
}

/*
Anima els characters, augmentant la variable de frame actual.
*/
void Map::animateCharacters(){
	chList.beginIteration();
	while (!chList.endIteration()) {
		Character* objecte;
		objecte = chList.nextElement();
		objecte->nextFrame();
	}
}


string Map::recorrerMapa()
{
	string ret = "";
	for (int i = 0; i < MAX_ROW_BLOCK; i++) {
		for (int j = 0; j < MAX_COL_BLOCK; j++) {
			ret += to_string(getCellType(i, j));
		}
	}
	return ret;
}
