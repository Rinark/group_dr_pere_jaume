#pragma once

//Third-party libraries
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <iostream>
#include <time.h>
#include <string>
#include "GameConstants.h"
#include "SDLInterface.h"
#include "InputManager.h"
#include "Sprite.h"
#include "Player.h"
#include "Map.h"
#include "Ranking.h"

using namespace std;


/*
* The Game class manages the game execution
*/
class Game {
	public:						
		Game(std::string windowTitle, int screenWidth, int screenHeight);	//Constructor
		~Game();															//Destructor
		void run();															//Game execution	

	private:
			//Attributes	
		std::string _windowTitle;		//SDLInterface Title
		int _screenWidth;				//Screen width in pixels				
		int _screenHeight;				//Screen height in pixels				
		int select;
		GameState _gameState;			//It describes the game state				
		SDLInterface _graphic;			//Manage the SDL graphic library		
		Map mapa;
		bool done;
		int timer;
		Ranking ranking;

			//Internal methods for the game execution
		void init();
		void gameLoop();
		void checkWinLose(Player * pl);
		void executePlayerCommands();
		void doPhysics();
		void renderGame();
		void renderWin();
		void renderLose();
		void renderActiveGame();
		void renderMenu();
		void drawSprite(Sprite & e);	
};

