#pragma once
#include <vector>
#include "Character.h"
#include "GenericList.h"
#include "Player.h"

class CharacterList: public GenericList<Character> {
	
public:
	CharacterList() {

	}
	~CharacterList() {
		cout << "Destroying Character List" << endl;
	}

	Player* GetHero() {
		if (0 >= _list.size()) {
			throw std::exception("[Get Element] The requested position does not exist");
		}
		return (Player*) _list[HERO_POSITION_LIST];
	}
};