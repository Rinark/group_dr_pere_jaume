#pragma once

//Game general information
#define GAME_SPEED 0.1f
#define GAME_TEXTURES 8

//Sprite information
#define SPRITE_SPEED 0.01f
#define SPRITE_DEFAULT_WIDTH 64
#define SPRITE_DEFAULT_HEIGHT 64
#define SPRITE_HERO 0
#define SPRITE_FIRE 1
#define SPRITE_WALL 2
#define SPRITE_GRASS 3
#define SPRITE_BOSS 4
#define SPRITE_TREE 5
#define SPRITE_BOTI 6
#define SPRITE_ARMOR 7

//Color information
#define GAME_BASIC_COLORS 5
#define RED 0
#define GREEN 1
#define BLUE 2
#define BLACK 3
#define WHITE 4

//Map information
#define MAX_ROW_BLOCK 20
#define MAX_COL_BLOCK 20
#define MAX_LAYERS 4

#define MAX_CHAR_NAME 8

//List fast positions
#define HERO_POSITION_LIST 0

//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{ INIT, INIT_PLAY, PLAY, EXIT, MENU, WIN, DESTROY, LOSE, S_RANKING, L_RANKING};

