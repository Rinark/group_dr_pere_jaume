#pragma once
#include "SDLInterface.h"
#include <fstream>
#include <string>
#include "Score.h"
#include "Array.h"
#include "SortAlgorithms.h"

using namespace std;

class Ranking{
public:
	Ranking();
	~Ranking();

	void initRanking();
	void saveRanking();
	void loadRanking();

	void saveUserName();
	void resetUser();
	void setPos(int posi);
	void setScore(int scoree);

	void drawL_Ranking(SDLInterface & _graphic, int _screenWidth, int _screenHeight);
	bool doneName();
	void drawS_Ranking(SDLInterface & _graphic, int _screenWidth, int _screenHeight);

private:
	int score;
	string tempUserName;
	int pos;
	Score ranking[10];
	char Abecedari[26] = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
};
