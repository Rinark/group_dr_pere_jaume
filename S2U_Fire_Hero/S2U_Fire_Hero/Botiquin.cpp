#include "Botiquin.h"

Botiquin::Botiquin(){
}

Botiquin::~Botiquin(){
}

Botiquin::Botiquin(int posX, int posY, int maxFrames) :Object(posX, posY, SPRITE_BOTI, maxFrames) {

}


void Botiquin::spawn(){
	srand((unsigned int)time(NULL));
	this->SetPosX(rand() % (MAX_COL_BLOCK - 2) + 1);
	this->SetPosY(rand() % (MAX_ROW_BLOCK - 2) + 1);
}