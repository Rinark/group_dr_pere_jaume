#pragma once
#include "Array.h"

/*********************
* This class contains several sorting methods. All methods are class methods (they are declared as static), which means that they can be called without declaring any object
* Be aware that the internal methods called by the sorting methods are private. This is because they have been created for being used only by the sorting methods
*********************/

template<class DataType>
class SortAlgorithms {
	/**
	* Swap two elements
	*@param a is the first element
	*@param b is the second element
	*/
	static void Swap(DataType& a, DataType& b) {
		DataType t;
		t = a;
		a = b;
		b = t;
	}

	/*
	* Walks an item down an array, as if it were a heap
	* @param p_array: the heap
	* @param p_index: index of the item to walk down
	* @param p_maxIndex: last valid index of the heap
	* @param p_compare: the comparison function
	*/
	static void HeapWalkDown(Array<DataType>& p_array, int p_index, int p_maxIndex, int(*p_compare)(DataType, DataType)) {
		int parent = p_index;
		int child = p_index * 2;
		DataType temp = p_array[parent - 1];

		// loop through, walking node down heap until both children are
		// smaller than node.
		while (child <= p_maxIndex)
		{
			// if left child is not the last node in the tree, then
			// find out which of the current node's children is largest.
			if (child < p_maxIndex)
			{
				if (p_compare(p_array[child - 1], p_array[child]) < 0)
				{   // change the pointer to the right child, since it is larger.
					child++;
				}
			}
			// if the node to walk down is lower than the highest value child,
			// move the child up one level.
			if (p_compare(temp, p_array[child - 1]) < 0)
			{
				p_array[parent - 1] = p_array[child - 1];
				parent = child;
				child *= 2;
			}
			else
				break;
		}
		p_array[parent - 1] = temp;
	}


	/**
	* Finds the median of three values in a segment
	* @param p_array: the array to find the median of
	* @param p_first: the first index in the segment
	* @param p_size: the size of the segment
	* @param p_compare: comparison function
	* @return the index of the median
	*/
	static	int FindMedianOfThree(Array<DataType>& p_array, int p_first, int p_size, int(*p_compare)(DataType, DataType)) {
		// calculate the last and middle indexes
		int last = p_first + p_size - 1;
		int mid = p_first + (p_size / 2);

		// if the first index is the lowest,
		if (p_compare(p_array[p_first], p_array[mid]) < 0 && p_compare(p_array[p_first], p_array[last]) < 0) {
			// then the smaller of the middle and the last is the median.
			if (p_compare(p_array[mid], p_array[last]) < 0)
				return mid;
			else
				return last;
		}

		// if the middle index is the lowest,
		if (p_compare(p_array[mid], p_array[p_first]) < 0 && p_compare(p_array[mid], p_array[last]) < 0) {
			// then the smaller of the first and last is the median
			if (p_compare(p_array[p_first], p_array[last]) < 0)
				return p_first;
			else
				return last;
		}

		// by this point, we know that the last index is the lowest,
		// so the smaller of the middle and the first is the median.
		if (p_compare(p_array[mid], p_array[p_first]) < 0)
			return mid;
		else
			return p_first;
	}

	/**
	* QuickSort algorithm (internal)
	* @param p_array: the array to sort
	* @param p_first: first index of the segment to sort
	* @param p_size: size of the segment
	* @param  p_compare: comparison function
	*/
	static void InternalQuickSort(Array<DataType>& p_array, int p_first, int p_size, int(*p_compare)(DataType, DataType)) {
		DataType pivot;
		int last = p_first + p_size - 1;    // index of the last cell
		int lower = p_first;                // index of the lower cell
		int higher = last;                  // index of the upper cell
		int mid;                            // index of the median value

											// if the size of the array to sort is greater than 1, then sort it.
		if (p_size > 1)
		{
			// find the index of the median value, and set that as the pivot.
			mid = FindMedianOfThree(p_array, p_first, p_size, p_compare);
			pivot = p_array[mid];

			// move the first value in the array into the place where the pivot was
			p_array[mid] = p_array[p_first];

			// while the lower index is lower than the higher index
			while (lower < higher)
			{
				// iterate downwards until a value lower than the pivot is found
				while (p_compare(pivot, p_array[higher]) < 0 && lower < higher)
					higher--;

				// if the previous loop found a value lower than the pivot, 
				// higher will not equal lower.
				if (higher != lower)
				{
					// so move the value of the higher index into the lower index 
					// (which is empty), and move the lower index up.
					p_array[lower] = p_array[higher];
					lower++;
				}

				// now iterate upwards until a value greater than the pivot is found
				while (p_compare(pivot, p_array[lower]) > 0 && lower < higher)
					lower++;

				// if the previous loop found a value greater than the pivot,
				// higher will not equal lower
				if (higher != lower)
				{
					// move the value at the lower index into the higher index,
					// (which is empty), and move the higher index down.
					p_array[higher] = p_array[lower];
					higher--;
				}
			}

			// at the end of the main loop, the lower index will be empty, so
			// put the pivot in there.
			p_array[lower] = pivot;

			// recursively quicksort the left half
			InternalQuickSort(p_array, p_first, lower - p_first, p_compare);

			// recursively quicksort the right half.
			InternalQuickSort(p_array, lower + 1, last - lower, p_compare);
		}
	}

public:

	/**
	* Bubble sorting algorithm
	* @param p_array is the array to sort
	* @param p_compare the is the comparison function
	*/
	static void BubbleSort(Array<DataType>& p_array, int(*p_compare)(DataType, DataType)) {
		int top = p_array.Size() - 1;
		int index;
		int swaps = 1;

		// loop through while the top is not 0 and swaps were made during the 
		// last iteration
		while (top != 0 && swaps != 0) {
			// set the swap variable to 0, because we haven't made any swaps yet.
			swaps = 0;

			// perform one iteration
			for (index = 0; index < top; index++) {
				// swap the indexes if necessary
				if (p_compare(p_array[index], p_array[index + 1]) > 0) {
					Swap(p_array[index], p_array[index + 1]);
					swaps++;
				}
			}
			top--;
		}
	}

	/**
	* Heap sorting algorithm
	* @param p_array is the array to sort
	* @param p_compare the is function passed to compare the elements
	*/
	static void HeapSort(Array<DataType>& p_array, int(*p_compare)(DataType, DataType)) {
		int i;
		int maxIndex = p_array.Size();
		int rightindex = maxIndex / 2;

		// walk everything down to transform it into a heap
		for (i = rightindex; i > 0; i--) {
			HeapWalkDown(p_array, i, maxIndex, p_compare);
		}

		// remove the top, walk everything down.
		while (maxIndex > 0) {
			Swap(p_array[0], p_array[maxIndex - 1]);
			maxIndex--;
			HeapWalkDown(p_array, 1, maxIndex, p_compare);
		}
	}

	/**
	* Quicksort algorithm
	* @param p_array: the array to sort
	* @param p_first: first index of the segment to sort
	* @param p_size: size of the segment
	* @param  p_compare: comparison function
	*/
	static void QuickSort(Array<DataType>& p_array, int(*p_compare)(DataType, DataType)) {
		InternalQuickSort(p_array, 0, p_array.m_size, p_compare);
	}



};
