#pragma once
#include "Character.h"

class Player : public Character {

public:
	Player();
	~Player();

	Player(int hp, int life, int posX, int posY, int maxFrames);

	int GetLifes();
	void SetLifes(int life);
	int GetScore();
	void SetScore(int sc);


	void spawn();

private:
	int lifes;
	int score;
};
