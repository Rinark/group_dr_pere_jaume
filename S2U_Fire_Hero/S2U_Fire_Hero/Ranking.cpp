#include "Ranking.h"

Ranking::Ranking(){
	pos = 0;
	tempUserName="";
}

Ranking::~Ranking(){

}

int compareScore(Score leftScore, Score rightScore) {
	if (leftScore.getScore() < rightScore.getScore())
		return 1;
	if (leftScore.getScore() > rightScore.getScore())
		return -1;
	return 0;
}


/*
Funcio per a generar un ranking de 10 persones per a fer tests.
*/
void Ranking::initRanking() {
	Score s;
	int sizeScore = sizeof(Score);
	std::ofstream myOutputFile;

	myOutputFile.open("Ranking", std::ios::out | std::ios::trunc | std::ios::binary);

	if (!myOutputFile.is_open()) {
		throw std::exception("[Ranking] System was not able to open the file");
	}

	for (int id = 0; id < 10; id++) {
		//agafem el array ordenat y el guardem al arxiu
		s.setRandomValues(id);
		myOutputFile.write((char *)(&s), sizeScore);
	}
	myOutputFile.close();
}

/*
Guarda i ordena el ranking.
*/
void Ranking::saveRanking() {
	std::ofstream myOutputFile;
	std::ifstream myInputFile;
	std::streampos fileSize;
	Score s;
	int sizeScore = sizeof(Score);

	myOutputFile.open("Ranking", std::ios::out | std::ios::trunc | std::ios::binary);

	//obrim el arxiu de ranking de entrada
	myInputFile.open("Ranking", std::ios::in | std::ios::binary);
	if (!myInputFile.is_open()) {
		throw std::exception("Ranking, System was not able to open the file");
	}
	//Compute the filesize
	myInputFile.seekg(0, std::ios::end);
	fileSize = myInputFile.tellg();
	myInputFile.seekg(0, std::ios::beg);

	//Read the content
	int numElements = (int)fileSize / sizeScore;

	/*creem el array a ordenar de Scores, es tamany dels elements presents
	+1 per al que afegirem aquesta vegada
	*/
	Array<Score> arraySortQuick(numElements + 1);
	
	//afegim el usuari que ha acabat el joc.
	s.setNom(tempUserName);
	s.setScore(score);
	arraySortQuick[0] = s;

	//Llegim el arxiu i guardem el ranking al array a ordenar. 
	for (int i = 0; i < numElements; i++) {
		myInputFile.read((char *)(&s), sizeScore);
		arraySortQuick[i + 1] = s;
	}
	myInputFile.close();

	//crida al Sorting algorithm quick sort per a que ordeni el array
	SortAlgorithms<Score>::QuickSort(arraySortQuick, compareScore);

	//obrim el arxiu de ranking de sortida en binary.
	if (!myOutputFile.is_open()) {
		throw std::exception("[Ranking] System was not able to open the file");
	}

	for (int id = 0; id < (numElements+1); id++){
		//agafem el array ordenat y el guardem al arxiu
		s = arraySortQuick[id];
		myOutputFile.write((char *)(&s), sizeScore);
	}
	myOutputFile.close();
}

/*
Carrega el ranking al array
*/
void Ranking::loadRanking() {
	std::ifstream myInputFile;
	std::streampos fileSize;
	Score s;
	int sizeScore = sizeof(Score);

	//Read the file with the random population
	myInputFile.open("Ranking", std::ios::in | std::ios::binary);
	if (!myInputFile.is_open()) {
		throw std::exception("Ranking, System was not able to open the file");
	}

	//Compute the filesize
	myInputFile.seekg(0, std::ios::end);
	fileSize = myInputFile.tellg();
	myInputFile.seekg(0, std::ios::beg);

	//Read the content
	int numElements = (int)fileSize / sizeScore;

	cout << numElements << " " << sizeof(ranking) << endl;
	for (int i = 0; i < 10; i++) {
		myInputFile.read((char *)(&s), sizeScore);
		ranking[i] = s;
	}

	myInputFile.close();
}

void Ranking::saveUserName() {
	tempUserName += Abecedari[pos];
}

void Ranking::resetUser() {
	pos = 0;
	tempUserName = "";
}

void Ranking::setPos(int posi){
	pos += posi;
	if (pos > sizeof(Abecedari)) pos=0;
	else if (pos < 0) pos = sizeof(Abecedari);
}

void Ranking::setScore(int scoree){
	score = scoree;
}

/*
Pinta el ranking.
*/
void Ranking::drawL_Ranking(SDLInterface& _graphic, int _screenWidth, int _screenHeight) {
	//Clear the screen
	_graphic.clearWindow();

	//Draw BG
	_graphic.drawFilledRectangle(BLACK, 0, 0, _screenWidth, _screenHeight);

	//Draw title
	_graphic.drawText("Ranking", WHITE, RED, (_screenWidth / 2) - 125, 10, 100);
	
	for (int i = 0; i < 10; i++){
		//Draw ranking
		string tempString = ranking[i].getNom() + " " + to_string(ranking[i].getScore());
		_graphic.drawText(tempString, WHITE, BLACK, (_screenWidth / 2) - 100, (150 + (30 * i)), 0);
	}
	//Refresh screen
	_graphic.refreshWindow();
}
bool Ranking::doneName() {
	if (tempUserName.size() < MAX_CHAR_NAME) {
		return false;
	}
	else return true;
}
void Ranking::drawS_Ranking(SDLInterface& _graphic, int _screenWidth, int _screenHeight) {
	//Clear the screen
	_graphic.clearWindow();

	//Draw BG
	_graphic.drawFilledRectangle(BLACK, 0, 0, _screenWidth, _screenHeight);

	//Draw title

	_graphic.drawText("Write your name:", WHITE, BLACK, (_screenWidth / 2) - 95, 200, 0);
	//calculem la mesura del string tempral del nom, quan sigui mes gran de la larga definida cridem a save ranking
	_graphic.drawText(tempUserName+Abecedari[pos], WHITE, BLACK, (_screenWidth / 2) - 95, 220, 0);

	//Refresh screen
	_graphic.refreshWindow();
}