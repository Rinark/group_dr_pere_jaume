#include "Game.h"


int main(int argc, char ** argv) {
	Game game("Basic game engine", 640, 640);

	try {
		game.run();
	}
	catch (std::exception e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
	}
	system("pause");
	return 0;
}