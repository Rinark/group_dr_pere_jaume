#include "Player.h"

Player::Player() {
}

Player::~Player() {
}

Player::Player(int hp, int life, int posX, int posY, int maxFrames) : Character(hp, posX, posY, SPRITE_HERO, maxFrames){
	lifes = life;
	score = 0;
}

int Player::GetLifes() {
	return lifes;
}

void Player::SetLifes(int life) {
	lifes = life;
}

int Player::GetScore() {
	return score;
}

void Player::SetScore(int sc) {
	score += sc;
}

void Player::spawn(){
}

