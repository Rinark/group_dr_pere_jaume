#pragma once
#include "Character.h"

class Boss : public Character
{
	public:
		Boss();
		~Boss();

		Boss(int hp, int posX, int posY, int maxFrames);

		void spawn();
	private:
};