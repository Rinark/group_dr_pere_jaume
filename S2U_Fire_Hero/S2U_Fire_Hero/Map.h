#pragma once
#include "Cell.h"
#include "GameConstants.h"
#include <fstream>
#include "GenericList.h"
#include "CharacterList.h"
#include "Object.h"
#include "Botiquin.h"
#include "Armadura.h"
#include "Enemic.h"
#include "Boss.h"
#include "Arbre.h"
#include "Wall.h"

class Map{
public:
	Map();
	~Map();

	void populateMap();
	void constructWalls();

	int getCellType(int x, int y);
	int getCellPosX(int x, int y);
	int getCellPosY(int x, int y);

	void saveGame();
	void loadMap();

	bool checkCollisions(int x, int y, int z);
	void executeObject(int obID, Object * objecte);

	void renderMap(SDLInterface & _graphic);
	void renderCharacters(int i, int j, int w, int z, SDLInterface & _graphic);
	void renderObjects(int i, int j, int w, int z, SDLInterface & _graphic);

	void animateObjects();
	void animateCharacters();

	Player* getHeroPointer();
	string recorrerMapa();
	void tractarString(string tempID);
private:

	Cell cells[MAX_ROW_BLOCK][MAX_COL_BLOCK];
	GenericList<Object> obList;
	CharacterList chList;
	char MAP_ID = '8';
};