#pragma once

class Cell {
public:
	Cell();
	Cell(int minx, int miny);
	~Cell();
private:
	friend class Map;
	int SpriteType;
	int minx, miny;
};