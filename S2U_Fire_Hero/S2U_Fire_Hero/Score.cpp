#include "Score.h"

Score::Score(){
}

Score::Score(Score & s) {
	strcpy_s(Nom, s.Nom);
	score = s.score;
}

Score::Score(string name, int puntuacio){
	strcpy_s(Nom, name.c_str());
	score = puntuacio;
}

Score::~Score(){
}

int Score::getScore(){
	return score;
}

void Score::setScore(int puntuacio){
	score = puntuacio;
}

string Score::getNom(){
	return string(Nom);
}

void Score::setNom(string name){
	strcpy_s(Nom, name.c_str());
}

void Score::setRandomValues(int id){
	string tempNom = "Person_" + to_string(id);
	strcpy_s(Nom, tempNom.c_str());
	score = abs(rand() %100);
}

std::ostream& operator<<(std::ostream& os, const Score& s) {
	os << "Nom:" << s.Nom << ", Score:" << s.score;
	return os;
}

std::istream& operator>>(std::istream& is, Score& s) {
	//Ask data to the user
	std::cout << "What's your name?";
	is >> s.Nom;
	std::cout << "How old are you?";
	is >> s.score;
	return is;
}


std::ofstream& operator<<(std::ofstream& os, const Score& s) {
	os << s.Nom << " ";
	os << s.score << " ";
	return os;
}

std::ifstream& operator>>(std::ifstream& is, Score& s) {
	is >> s.Nom;
	is >> s.score;
	return is;
}