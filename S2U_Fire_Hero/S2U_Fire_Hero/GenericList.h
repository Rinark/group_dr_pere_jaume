#pragma once
#include <vector>

template <class Data>

class GenericList{
protected:
	std::vector<Data *> _list;

	int _iterator;

public:
	GenericList() {

	}
	~GenericList() {
		cout << "Deleting genericList" << endl;
		for (unsigned int i = 0; i < _list.size(); i++) {
			Data * element = _list[i];
			delete _list[i];
			_list.pop_back();
		}
		_iterator = 0;
	}

	void emptyList() {
		for (unsigned int i = 0; i < _list.size(); i++) {
			Data * element = _list[i];
			_list.pop_back();
		}
	}

	//ADD
	void addElement(Data * d) {
		_list.push_back(d);
	}

	int returnIterator() {
		return (_iterator-1);
	}

	int returnSize() {
		return _list.size();
	}
	//moves all the vector positions
	/*void addElementShift(Data * d, int position) {
		_list.resize(_list.size() + 1);
		//homework
	}*/
	//replaces the information (pointer adress)
	void addElementReplace(Data * d, int position) {
		Data * element = _list[position];
		delete element;
		_list[position] = d;
	}
	//REMOVE LAST ELEMENT
	void removeElement() {
		if (_iterator > 0) {
			_list.erase((_list.begin() + (_iterator - 1)));
		}
		else{
			_list.erase((_list.begin() + _iterator));
		}
	}
	//Fer funcio per eliminar el ultim element

	void removeElement(int position) {
		if (position >= _list.size()) {
			throw std::exception("[List remove] We cannot delete a position which doesn't exist");
		}
		Data * element = _list[position];
		delete element;
	}

	//Begin iteration
	void beginIteration() {
		_iterator = 0;
	}
	//Next element
	Data * nextElement() {
		_iterator++;
		if (_iterator > _list.size()) throw std::exception("[GenericList :: Next Element] Out of range");
		return _list[_iterator - 1];
	}
	//End iteration
	bool endIteration() {
		if (_iterator == _list.size()) return true;
		else return false;
	}
	//Get element

	Data * getElement(int position) {
		if (position >= _list.size()) {
			throw std::exception("[Get Element] The requested position does not exist");
		}
		return _list[position];
	}

	//PRINT
	void printList() {

	}

	/*friend ostream& operator<<(ostream& os, const GenericList& l){
	l.beginIteration();
	while (!l.endIteration()) {
	Data * element = l.getElement();
	os <<
	}
	}*/
	};