#pragma once
#include "Element.h"

class Object: public Element{
public:
	Object();
	~Object();

	Object(int posX, int posY, int id, int maxFrames);
	
	virtual void spawn() = 0;

	private:
};


