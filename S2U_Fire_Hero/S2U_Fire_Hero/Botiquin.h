#pragma once
#include "Object.h"
#include <time.h>

class Botiquin : public Object{
public:
	Botiquin();
	~Botiquin();

	Botiquin(int posX, int posY, int maxFrames);

	void spawn();

private:

};
