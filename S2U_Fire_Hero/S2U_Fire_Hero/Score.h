#pragma once
#include <string>			
#include <iostream>						
#include <fstream>

using namespace std;

class Score{
public:
	Score();
	Score(Score & s);
	Score(string name, int puntuacio);
	~Score();

	int getScore();
	void setScore(int puntuacio);

	string getNom();
	void setNom(string name);

	void setRandomValues(int id);

	friend std::ostream& operator<<(std::ostream& os, const Score& s);
	friend std::istream& operator>>(std::istream& is, Score& s);

	friend std::ofstream& operator<<(std::ofstream& os, const Score& s);
	friend std::ifstream& operator>>(std::ifstream& is, Score& s);

private:
	char Nom[10];
	int score;
};

