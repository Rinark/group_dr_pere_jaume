#include "Character.h"

Character::Character ()
{
}

Character::~Character()
{
}

Character::Character(int hp, int posX, int posY, int id, int maxFrames) :Element(posX, posY, id, maxFrames) {
	health = hp;
	armor = 0;
}

int Character::GetHealth() {
	return health;
}


void Character::SetHealth(int hp) {
	health += hp;
}

int Character::GetArmor() {
	return armor;
}

void Character::SetArmor(int Armor) {
	armor += Armor;
	cout << armor << endl;
}

/*
Returns the size of Inventory.
*/
int Character::GetInventorySize(){
	return Inventory.returnSize();
}

void Character::RemoveElementInventory() {
	Inventory.removeElement();
}

void Character::AddElementInventory(Object* objecte){
	Inventory.addElement(objecte);
}

void Character::emptyInventory(){
	Inventory.emptyList();
}


int Character::ReturnElementInventory(int position) {
	Object* item = Inventory.getElement(position);
	return item->GetID();
}
