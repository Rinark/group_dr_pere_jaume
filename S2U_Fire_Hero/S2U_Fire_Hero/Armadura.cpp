#include "Armadura.h"

Armadura::Armadura(){
}

Armadura::~Armadura(){
}

Armadura::Armadura(int posX, int posY, int maxFrames) :Object(posX, posY, SPRITE_ARMOR, maxFrames) {

}


void Armadura::spawn(){
	srand((unsigned int)time(NULL));
	this->SetPosX(rand() % (MAX_COL_BLOCK - 2) + 1);
	this->SetPosY(rand() % (MAX_ROW_BLOCK - 2) + 1);
}
