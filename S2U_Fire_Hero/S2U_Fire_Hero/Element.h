#pragma once
#include "SDLInterface.h"
#include <iostream>
#include <string>
#include "GenericList.h"
using namespace std;

class Element {
public:
	Element ();
	~Element ();

	Element(int posX, int posY, int id, int maxFrames);

	int GetPosX();
	void SetPosX(int X);
	int GetPosY();
	void SetPosY(int Y);
	int GetID();
	void SetID();
	void setCurrentFrame(int newCurrentFrame);
	int getCurrentFrame();
	void nextFrame();
	int getMaxFrames();

	virtual void spawn() = 0;//Ara mateix esta buit pero l'implementarem a la seguent release
	void draw(SDLInterface& _graphic, int z, int w);

private:
	int _currentFrame;
	int _maxFrames;
	int x;
	int y;
	int ID;
};

