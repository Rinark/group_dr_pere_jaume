#pragma once
#include "Element.h"
#include "Object.h"

class Character: public Element {
public:
	Character();
	~Character();

	Character(int hp, int posX, int posY, int id, int maxFrames);

	int GetHealth();
	void SetHealth(int hp);

	int GetArmor();
	void SetArmor(int armor);

	int GetInventorySize();

	void RemoveElementInventory();
	void AddElementInventory(Object* objecte);
	int ReturnElementInventory(int position);
	void emptyInventory();

	virtual void spawn() = 0;

private:
	int health;
	int armor;
	GenericList<Object> Inventory;
};