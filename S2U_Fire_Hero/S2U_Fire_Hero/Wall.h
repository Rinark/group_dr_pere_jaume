#pragma once
#include "Object.h"

class Wall : public Object{
public:
	Wall();
	~Wall();

	Wall(int posX, int posY, int maxFrames);

	void spawn();

private:
};

