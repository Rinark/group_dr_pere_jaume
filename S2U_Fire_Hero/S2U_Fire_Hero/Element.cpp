#include "Element.h"

Element::Element(){
}

Element ::~Element(){
}


Element::Element(int posX, int posY, int id, int maxFrames){
	x = posX;
	y = posY;
	ID = id;
	_maxFrames = maxFrames-1;
	_currentFrame = 0;
}

int Element::GetPosX() {
	return x;
}

void Element::SetPosX(int PosX) {
	x = PosX;
}

int Element::GetPosY() {
	return y;
}

void Element::SetPosY(int PosY) {
	y = PosY;
}

int Element::GetID(){
	return ID;
}

void Element::SetID()
{
}


void Element::draw(SDLInterface& _graphic, int z, int w){
	_graphic.drawTexture(ID, SPRITE_DEFAULT_WIDTH * _currentFrame, 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT, 64 * z, 64 * w, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
}

int Element::getCurrentFrame() {
	return _currentFrame;
}

void Element::setCurrentFrame(int newCurrentFrame) {
	_currentFrame = newCurrentFrame;
}

void Element::nextFrame() {
	if ((_currentFrame + 1)<=_maxFrames){
		_currentFrame += 1;
	}
	else{
		_currentFrame = 0;
	}
}

int Element::getMaxFrames() {
	return _maxFrames;
}
